export interface EdicionTrabajador {
     IDTrabajador: string;
     nombre: string;
     apellidos: string;
     diasLaborales: string;
     telefono: string;
}
