export interface ListadoTrabajadores {
     departamentoid: number;
     IDTrabajador: string;
     nombre: string;
     apellidos: string;
     diasLaborales: string;
     telefono: string;
}
