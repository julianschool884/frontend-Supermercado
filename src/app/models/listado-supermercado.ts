export interface ListadoSupermercado {
     razonSocial: string;
     estado: string;
     correo: string;
     telefono: string;
}
