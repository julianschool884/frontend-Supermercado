export interface RegistroSupermercado {
         idencargado: number;
         nombreSupermercado: string;
         calle: string;
         numero: string;
         codigoPostal: string;
         coloniaDelegacion: string;
         estado: string;
         ciudad: string;
         razonSocial: string;
         correo: string;
         telefono: string;
}
