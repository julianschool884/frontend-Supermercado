export interface ListadoEncargado {
     nombre: string;
     apellidoP: string;
     apellidoM: string;
     fecha_Nacimiento: string;
     genero: string;
     correo: string;
     telefono: string;
}
