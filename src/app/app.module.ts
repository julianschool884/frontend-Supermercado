import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './pages/navbar/navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Navbar2Component } from './pages/navbar2/navbar2.component';
import { ListadoEncargadoComponent } from './pages/SuperAdmin/listado-encargado/listado-encargado.component';
import { RegistroEncargadoComponent } from './pages/SuperAdmin/registro-encargado/registro-encargado.component';
import { EdicionEncargadoComponent } from './pages/SuperAdmin/edicion-encargado/edicion-encargado.component';
import { SiderbarComponent } from './pages/siderbar/siderbar.component';
import { SidebarModule } from 'ng-sidebar';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ListadoSupermercadoComponent } from './pages/SuperAdmin/listado-supermercado/listado-supermercado.component';
import { RegistroSupermercadoComponent } from './pages/Encargado/registro-supermercado/registro-supermercado.component';
import { ListadoDepartamentosComponent } from './pages/Encargado/listado-departamentos/listado-departamentos.component';
import { RegistroDepartamentosComponent } from './pages/Encargado/registro-departamentos/registro-departamentos.component';
import { ListadoTrabajadoresComponent } from './pages/Encargado/listado-trabajadores/listado-trabajadores.component';
import { RegistroTrabajadoresComponent } from './pages/Encargado/registro-trabajadores/registro-trabajadores.component';
import { EdicionTrabajadorComponent } from './pages/Encargado/edicion-trabajador/edicion-trabajador.component';
import { Siderbar2Component } from './pages/siderbar2/siderbar2.component'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    LoginComponent,
    Navbar2Component,
    ListadoEncargadoComponent,
    RegistroEncargadoComponent,
    EdicionEncargadoComponent,
    SiderbarComponent,
    ListadoSupermercadoComponent,
    RegistroSupermercadoComponent,
    ListadoDepartamentosComponent,
    RegistroDepartamentosComponent,
    ListadoTrabajadoresComponent,
    RegistroTrabajadoresComponent,
    EdicionTrabajadorComponent,
    Siderbar2Component,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SidebarModule.forRoot(),
    ReactiveFormsModule,


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  hide = true;
 }
