import { TestBed } from '@angular/core/testing';

import { ListadoEncargadoService } from './listado-encargado.service';

describe('ListadoEncargadoService', () => {
  let service: ListadoEncargadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListadoEncargadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
