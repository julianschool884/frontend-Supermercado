import { TestBed } from '@angular/core/testing';

import { ListadoSupermercadoService } from './listado-supermercado.service';

describe('ListadoSupermercadoService', () => {
  let service: ListadoSupermercadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListadoSupermercadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
