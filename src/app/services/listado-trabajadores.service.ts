import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListadoTrabajadoresService {
  private MyBaseUrl: string = environment.BASE_API_URL;
  constructor( private readonly http: HttpClient) { }

  public getlistadoTrabajadores(): Observable <Object>{
    return this.http.get<ListadoTrabajadoresService[]>(this.MyBaseUrl + "ListadoTrabajadores", {responseType: "json"})
  }

  public getnombreDepartamento(): Observable <Object>{
    return this.http.get<ListadoTrabajadoresService[]>(this.MyBaseUrl + "ListadoDepartamentos", {responseType: "json"})
  }
}
