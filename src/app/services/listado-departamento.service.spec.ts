import { TestBed } from '@angular/core/testing';

import { ListadoDepartamentoService } from './listado-departamento.service';

describe('ListadoDepartamentoService', () => {
  let service: ListadoDepartamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListadoDepartamentoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
