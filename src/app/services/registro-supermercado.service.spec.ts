import { TestBed } from '@angular/core/testing';

import { RegistroSupermercadoService } from './registro-supermercado.service';

describe('RegistroSupermercadoService', () => {
  let service: RegistroSupermercadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RegistroSupermercadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
