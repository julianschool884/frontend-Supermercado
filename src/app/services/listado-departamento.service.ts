import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListadoDepartamentoService {
  private MyBaseUrl: string = environment.BASE_API_URL;
  constructor( private readonly http: HttpClient) { }

  public getlistadoDepartamento(): Observable <Object>{
    return this.http.get<ListadoDepartamentoService[]>(this.MyBaseUrl + "ListadoDepartamentos", {responseType: "json"})
  }
}
