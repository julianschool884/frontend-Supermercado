import { TestBed } from '@angular/core/testing';

import { ListadoTrabajadoresService } from './listado-trabajadores.service';

describe('ListadoTrabajadoresService', () => {
  let service: ListadoTrabajadoresService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListadoTrabajadoresService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
