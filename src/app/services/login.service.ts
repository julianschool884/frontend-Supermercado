import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  private MyBaseUrl: string = environment.BASE_API_URL;

  constructor( private readonly http: HttpClient) { }


  login(auth:any){
    return this.http.post(this.MyBaseUrl+'Login',auth);
  }

  ejemploGet(){
    return this.http.get(this.MyBaseUrl+'ListadoEncargados');
  }
}
