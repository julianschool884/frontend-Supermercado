import { TestBed } from '@angular/core/testing';

import { EdicionTrabajadorService } from './edicion-trabajador.service';

describe('EdicionTrabajadorService', () => {
  let service: EdicionTrabajadorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EdicionTrabajadorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
