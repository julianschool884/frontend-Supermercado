import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class RegistroSupermercadoService {
  private MyBaseUrl: string = environment.BASE_API_URL;

  constructor( private readonly http: HttpClient) { }

  
  registrar(auth:any){
    return this.http.post(this.MyBaseUrl+'RegistroSupermercado',auth);
  }
}
