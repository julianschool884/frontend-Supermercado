import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListadoEncargadoService {
  private MyBaseUrl: string = environment.BASE_API_URL;
  constructor( private readonly http: HttpClient) { }

  public getlistadoEncagado(): Observable <Object>{
    return this.http.get<ListadoEncargadoService[]>(this.MyBaseUrl + "ListadoEncargados", {responseType: "json"})
  }

  public getnombreSupermecado(): Observable <Object>{
    return this.http.get<ListadoEncargadoService[]>(this.MyBaseUrl + "ListadoSupermercado", {responseType: "json"})
  }

}
