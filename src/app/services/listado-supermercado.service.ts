import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListadoSupermercadoService {
  private MyBaseUrl: string = environment.BASE_API_URL;
  constructor( private readonly http: HttpClient) { }

  public getlistadoSupermercado(): Observable <Object>{
    return this.http.get<ListadoSupermercadoService[]>(this.MyBaseUrl + "ListadoSupermercado", {responseType: "json"})
  }

}
