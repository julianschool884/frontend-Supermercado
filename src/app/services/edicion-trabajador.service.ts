import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EdicionTrabajadorService {
  
  private MyBaseUrl: string = environment.BASE_API_URL;
  constructor( private readonly http: HttpClient) { }

  public getnombreDepartamento(): Observable <Object>{
    return this.http.get<EdicionTrabajadorService[]>(this.MyBaseUrl + "ListadoDepartamentos", {responseType: "json"})
  }

  editar(id: number, body: any){
    return this.http.put(this.MyBaseUrl+'ActulizarTrabajador/'+id, body);
  }
}
