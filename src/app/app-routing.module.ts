import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ListadoEncargadoComponent } from './pages/SuperAdmin/listado-encargado/listado-encargado.component';
import { RegistroEncargadoComponent } from './pages/SuperAdmin/registro-encargado/registro-encargado.component';
import { EdicionEncargadoComponent } from './pages/SuperAdmin/edicion-encargado/edicion-encargado.component';
import { ListadoSupermercadoComponent } from './pages/SuperAdmin/listado-supermercado/listado-supermercado.component';
import { RegistroSupermercadoComponent } from './pages/Encargado/registro-supermercado/registro-supermercado.component'
import { ListadoDepartamentosComponent } from './pages/Encargado/listado-departamentos/listado-departamentos.component'
import { ListadoTrabajadoresComponent } from './pages/Encargado/listado-trabajadores/listado-trabajadores.component'
import { RegistroTrabajadoresComponent } from './pages/Encargado/registro-trabajadores/registro-trabajadores.component'
import { EdicionTrabajadorComponent } from './pages/Encargado/edicion-trabajador/edicion-trabajador.component'

const routes: Routes = [
  { path: 'home', component:HomeComponent },
  { path: 'login', component:LoginComponent },
  { path: 'listadoEncargado', component:ListadoEncargadoComponent },
  { path: 'registroEncargado', component:RegistroEncargadoComponent },
  { path: 'edicionEncargado', component:EdicionEncargadoComponent },
  { path: 'listadoSupermercado', component:ListadoSupermercadoComponent },
  { path: 'registroSupermercado', component:RegistroSupermercadoComponent },
  { path: 'listadoDepartamentos', component:ListadoDepartamentosComponent },
  { path: 'listadoTrabajadores', component:ListadoTrabajadoresComponent },
  { path: 'registroTrabajadores', component:RegistroTrabajadoresComponent },
  { path: 'edicionTrabajadores', component:EdicionTrabajadorComponent },


  {
    path: "**",
    pathMatch: "full",
    redirectTo: "home"
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

