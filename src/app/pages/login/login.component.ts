import { Component, Input, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from "sweetalert2";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() login: any={};
  formularioLogin: any;

  constructor(
    private readonly loginService:LoginService,
    private form_builder: FormBuilder,
    private router : Router
  ) { }

  ngOnInit(): void {
    this.formularioLogin = this.form_builder.group({
      email: ['', [Validators.required, Validators.email]],
      contrasena: ['', Validators.required],
    });
  }

  loginbtn(){
    if (this.formularioLogin.valid) {
      const emailControl = this.formularioLogin.get('email').value;
      const contrasenaControl = this.formularioLogin.get('contrasena').value;

      const auth = {
        correo: emailControl,
        contrasena: contrasenaControl
      };

      this.loginService.login(auth).subscribe((respuesta: any) => { 
        Swal.fire({
          icon: 'success',
          title: 'Exito',
          text: 'Bienvenido!',
        });

        if (respuesta.tipoUsuario === "SuperAdmin") {
          setTimeout(()=>{
            Swal.close();
            this.router.navigate(['/listadoEncargado']);
          },5000); 
        }else{
          setTimeout(()=>{
            Swal.close();
            this.router.navigate(['/registroSupermercado']);
          },5000); 
        }
        console.log(respuesta);
        
      
      },
        error =>{console.log('Salio Mal',error);
        }
      );

      console.log(emailControl, contrasenaControl);
    } else {

      Swal.fire({
        icon: 'error',
        title: 'Error Comprueba que tus datos sean los correctos',
        text: 'contraseña o correo estan mal escritos!',
      });
      console.log('false');
      
    }
  
  }
}
