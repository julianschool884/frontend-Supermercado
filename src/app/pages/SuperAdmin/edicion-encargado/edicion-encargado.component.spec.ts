import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdicionEncargadoComponent } from './edicion-encargado.component';

describe('EdicionEncargadoComponent', () => {
  let component: EdicionEncargadoComponent;
  let fixture: ComponentFixture<EdicionEncargadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdicionEncargadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdicionEncargadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
