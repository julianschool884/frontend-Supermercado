import { Component, OnInit } from '@angular/core';
import { ListadoSupermercadoService } from '../../../services/listado-supermercado.service'
import { ListadoSupermercado } from '../../../models/listado-supermercado'

@Component({
  selector: 'app-listado-supermercado',
  templateUrl: './listado-supermercado.component.html',
  styleUrls: ['./listado-supermercado.component.css']
})
export class ListadoSupermercadoComponent implements OnInit {
  public ListadoSupermercados?: ListadoSupermercado[];
  private _activatedRoute: any;

  constructor( public listadoSupermercadoService:ListadoSupermercadoService) { }

  ngOnInit(): void {
    this.listadoSupermercadoService.getlistadoSupermercado().subscribe(
      ListadoSupermercados => {
        this.ListadoSupermercados = (<ListadoSupermercado[]> ListadoSupermercados);
      })
  }

}
