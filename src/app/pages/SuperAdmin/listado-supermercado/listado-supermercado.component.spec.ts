import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoSupermercadoComponent } from './listado-supermercado.component';

describe('ListadoSupermercadoComponent', () => {
  let component: ListadoSupermercadoComponent;
  let fixture: ComponentFixture<ListadoSupermercadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListadoSupermercadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoSupermercadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
