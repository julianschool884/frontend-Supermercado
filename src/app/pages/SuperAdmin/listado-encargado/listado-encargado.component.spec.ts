import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoEncargadoComponent } from './listado-encargado.component';

describe('ListadoEncargadoComponent', () => {
  let component: ListadoEncargadoComponent;
  let fixture: ComponentFixture<ListadoEncargadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListadoEncargadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoEncargadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
