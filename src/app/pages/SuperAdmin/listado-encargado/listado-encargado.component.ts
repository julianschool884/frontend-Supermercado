import { Component, OnInit } from '@angular/core';
import { ListadoEncargado } from '../../../models/listado-encargado'
import { RegistroSupermercado } from '../../../models/registro-supermercado'
import { ListadoEncargadoService } from '../../../services/listado-encargado.service'

@Component({
  selector: 'app-listado-encargado',
  templateUrl: './listado-encargado.component.html',
  styleUrls: ['./listado-encargado.component.css']
})
export class ListadoEncargadoComponent implements OnInit {
  public ListadoEncargados?: ListadoEncargado[];
  public RegistroSupermercados?: RegistroSupermercado[];
  private _activatedRoute: any;


  constructor(
    public listadoEncargadoService:ListadoEncargadoService
  ) { }

  ngOnInit(): void {
    this.listadoEncargadoService.getlistadoEncagado().subscribe(
      ListadoEncargados => {
        this.ListadoEncargados = (<ListadoEncargado[]> ListadoEncargados);
      })

      this.listadoEncargadoService.getnombreSupermecado().subscribe(
        RegistroSupermercados => {
          this.RegistroSupermercados = (<RegistroSupermercado[]>RegistroSupermercados);
        }
      )


    }
  }
