import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Siderbar2Component } from './siderbar2.component';

describe('Siderbar2Component', () => {
  let component: Siderbar2Component;
  let fixture: ComponentFixture<Siderbar2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Siderbar2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Siderbar2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
