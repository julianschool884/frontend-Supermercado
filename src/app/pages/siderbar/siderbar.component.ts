import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-siderbar',
  templateUrl: './siderbar.component.html',
  styleUrls: ['./siderbar.component.css']
})
export class SiderbarComponent implements OnInit {

  opened = false;

  constructor() { }

  ngOnInit(): void {
  }
 
  toggleSidebar() {
    this.opened = !this.opened;
  }

}
