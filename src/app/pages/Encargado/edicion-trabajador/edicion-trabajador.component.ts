import { Component, OnInit } from '@angular/core';
import { EdicionTrabajadorService } from '../../../services/edicion-trabajador.service'
import { ListadoDepartamento } from '../../../models/listado-departamento'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from "sweetalert2";


@Component({
  selector: 'app-edicion-trabajador',
  templateUrl: './edicion-trabajador.component.html',
  styleUrls: ['./edicion-trabajador.component.css']
})
export class EdicionTrabajadorComponent implements OnInit {
  formularioEditar: any;

  public ListadoDepartamentos?: ListadoDepartamento[];

  constructor(
    public edicionTrabajadorService:EdicionTrabajadorService,
    private form_builder: FormBuilder,
    private router : Router
    ) { }

  ngOnInit(): void {
    this.edicionTrabajadorService.getnombreDepartamento().subscribe(
      ListadoDepartamentos => {
        this.ListadoDepartamentos = (<ListadoDepartamento[]>ListadoDepartamentos);
      }
    )

    
     this.formularioEditar = this.form_builder.group({
      departamento: ['', Validators.required],
      idUser: ['', Validators.required],

      nombre: ['', Validators.required],

      apellido: ['', Validators.required],

      diasLaborales: ['', Validators.required],

      telefono: ['', Validators.required],
     })
    }

    editarbtn(){
      if (this.formularioEditar.valid) {
        const departamento = this.formularioEditar.get('departamento').value;
        const idUser = this.formularioEditar.get('idUser').value;
        const nombre = this.formularioEditar.get('nombre').value;
        const apellido = this.formularioEditar.get('apellido').value;
        const diasLaborales = this.formularioEditar.get('diasLaborales').value;
        const telefono = this.formularioEditar.get('telefono').value;

  
        const ediconTrabajador = {
            IDTrabajador:idUser,
            nombre:nombre,
            apellidos:apellido,
            diasLaborales:diasLaborales,
            telefono:telefono
        
        };
  
        this.edicionTrabajadorService.editar(idUser,ediconTrabajador).subscribe((respuesta: any) => { 
          Swal.fire({
            icon: 'success',
            title: 'Exito',
            text: 'Bienvenido!',
          });
  
          if (respuesta.tipoUsuario === "SuperAdmin") {
            setTimeout(()=>{
              Swal.close();
              this.router.navigate(['/listadoEncargado']);
            },5000); 
          }else{
            setTimeout(()=>{
              Swal.close();
              this.router.navigate(['/registroSupermercado']);
            },5000); 
          }
          console.log(respuesta); 
          
         /*  Swal.fire({
            icon: 'success',
            title: 'Exito',
            text: 'Bienvenido!',
          });
          setTimeout(()=>{
            Swal.close();
            this.router.navigate([http://localhost:4200/listadoEncargado]);
          },5000);
          },
          error =>{console.log('Salio Mal',error);
          }
        );
  
   /*    } else {
        console.log('false'); */
        
      }

        )}}}