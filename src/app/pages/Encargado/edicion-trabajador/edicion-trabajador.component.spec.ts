import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdicionTrabajadorComponent } from './edicion-trabajador.component';

describe('EdicionTrabajadorComponent', () => {
  let component: EdicionTrabajadorComponent;
  let fixture: ComponentFixture<EdicionTrabajadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdicionTrabajadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdicionTrabajadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
