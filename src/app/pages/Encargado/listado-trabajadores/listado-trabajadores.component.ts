import { Component, OnInit } from '@angular/core';
import { ListadoTrabajadores } from '../../../models/listado-trabajadores'
import { ListadoTrabajadoresService } from '../../../services/listado-trabajadores.service'
import { ListadoDepartamento } from '../../../models/listado-departamento'

@Component({
  selector: 'app-listado-trabajadores',
  templateUrl: './listado-trabajadores.component.html',
  styleUrls: ['./listado-trabajadores.component.css']
})
export class ListadoTrabajadoresComponent implements OnInit {
  public ListadoTrabajadores2?: ListadoTrabajadores[];
  public ListadoDepartamentos?: ListadoDepartamento[];
  private _activatedRoute: any;
  
  constructor(public listadoTrabajadoresService:ListadoTrabajadoresService) { }

  ngOnInit(): void {
    this.listadoTrabajadoresService.getlistadoTrabajadores().subscribe(
      ListadoTrabajadores2 => {
        this.ListadoTrabajadores2 = (<ListadoTrabajadores[]> ListadoTrabajadores2);
      })

      this.listadoTrabajadoresService.getnombreDepartamento().subscribe(
        ListadoDepartamentos => {
          this.ListadoDepartamentos = (<ListadoDepartamento[]>ListadoDepartamentos);
        }
      )
  }

}
