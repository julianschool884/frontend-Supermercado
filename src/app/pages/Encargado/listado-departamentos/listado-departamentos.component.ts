import { Component, OnInit } from '@angular/core';
import { ListadoDepartamento } from '../../../models/listado-departamento'
import { ListadoDepartamentoService } from '../../../services/listado-departamento.service'

@Component({
  selector: 'app-listado-departamentos',
  templateUrl: './listado-departamentos.component.html',
  styleUrls: ['./listado-departamentos.component.css']
})
export class ListadoDepartamentosComponent implements OnInit {
  public ListadoDepartamentos?: ListadoDepartamento[];
  private _activatedRoute: any;

  constructor(
    public listadoDepartamentoService:ListadoDepartamentoService
  ) { }

  ngOnInit(): void {
    this.listadoDepartamentoService.getlistadoDepartamento().subscribe(
      ListadoDepartamentos => {
        this.ListadoDepartamentos = (<ListadoDepartamento[]> ListadoDepartamentos);
      })
  }

}
