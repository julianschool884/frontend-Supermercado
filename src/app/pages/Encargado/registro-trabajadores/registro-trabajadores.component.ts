import { Component, OnInit } from '@angular/core';
import { ListadoDepartamento } from '../../../models/listado-departamento'
import { RegistroTrabajadorService } from '../../../services/registro-trabajador.service'


@Component({
  selector: 'app-registro-trabajadores',
  templateUrl: './registro-trabajadores.component.html',
  styleUrls: ['./registro-trabajadores.component.css']
})

export class RegistroTrabajadoresComponent implements OnInit {
  public ListadoDepartamentos?: ListadoDepartamento[];

  constructor(public registroTrabajadorService:RegistroTrabajadorService) { }

  ngOnInit(): void {
    this.registroTrabajadorService.getnombreDepartamento().subscribe(
      ListadoDepartamentos => {
        this.ListadoDepartamentos = (<ListadoDepartamento[]>ListadoDepartamentos);
      }
    )
  }

}
