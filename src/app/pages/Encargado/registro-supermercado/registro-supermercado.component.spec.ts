import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroSupermercadoComponent } from './registro-supermercado.component';

describe('RegistroSupermercadoComponent', () => {
  let component: RegistroSupermercadoComponent;
  let fixture: ComponentFixture<RegistroSupermercadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroSupermercadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroSupermercadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
