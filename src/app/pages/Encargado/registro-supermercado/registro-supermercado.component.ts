import { Component, OnInit } from '@angular/core';
import { RegistroSupermercadoService } from '../../../services/registro-supermercado.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from "sweetalert2";

@Component({
  selector: 'app-registro-supermercado',
  templateUrl: './registro-supermercado.component.html',
  styleUrls: ['./registro-supermercado.component.css']
})
export class RegistroSupermercadoComponent implements OnInit {
  formularioRegistrar: any;

  constructor(
    public registroSupermercadoService:RegistroSupermercadoService,
    private form_builder: FormBuilder,
    private router : Router
  ) { }

  ngOnInit(): void {

    this.formularioRegistrar = this.form_builder.group({
      nombreSupermercado: ['', Validators.required],
      calle: ['', Validators.required],
      numero: ['', Validators.required],
      codigoPostal: ['', Validators.required],
      colonia: ['', Validators.required],
      estado: ['', Validators.required],
      ciudad: ['', Validators.required],
      razonSocial: ['', Validators.required],
      correo: ['', Validators.required],
      telefono: ['', Validators.required],
     })
  }

  registrarbtn(){
    if (this.formularioRegistrar.valid) {
      const nombreSupermercado = this.formularioRegistrar.get('nombreSupermercado').value;
      const calle = this.formularioRegistrar.get('calle').value;
      const numero = this.formularioRegistrar.get('numero').value;
      const codigoPostal = this.formularioRegistrar.get('codigoPostal').value;
      const colonia = this.formularioRegistrar.get('colonia').value;
      const estado = this.formularioRegistrar.get('estado').value;
      const ciudad = this.formularioRegistrar.get('calle').ciudad;
      const razonSocial = this.formularioRegistrar.get('razonSocial').value;
      const correo = this.formularioRegistrar.get('correo').value;
      const telefono = this.formularioRegistrar.get('telefono').value;


  



      }}}
