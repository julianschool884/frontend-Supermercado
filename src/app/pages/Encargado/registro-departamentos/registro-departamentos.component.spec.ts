import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroDepartamentosComponent } from './registro-departamentos.component';

describe('RegistroDepartamentosComponent', () => {
  let component: RegistroDepartamentosComponent;
  let fixture: ComponentFixture<RegistroDepartamentosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroDepartamentosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroDepartamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
